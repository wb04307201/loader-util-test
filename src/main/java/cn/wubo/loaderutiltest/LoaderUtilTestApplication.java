package cn.wubo.loaderutiltest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan(value = {"cn.wubo"})
@SpringBootApplication
public class LoaderUtilTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(LoaderUtilTestApplication.class, args);
    }

}
