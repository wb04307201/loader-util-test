package cn.wubo.loaderutiltest;

import cn.wubo.loader.util.LoaderUtils;
import cn.wubo.loader.util.MethodUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.lang.reflect.InvocationTargetException;

@RequestMapping(value = "/test")
@RestController
public class TestController {

    private static final String methodName = "testMethod";
    private static final String fullClassName = "cn.wubo.loaderutiltest.TestClass";
    private static final String javaSourceCode = """
            package cn.wubo.loaderutiltest;
                                
                public class TestClass {
                                
                    public String testMethod(String name){
                        return String.format("Hello,%s!",name);
                    }
                }
            """;

    private static final String fullClassNameBean = "cn.wubo.loaderutiltest.DemoController";
    private static final String javaSourceCodeController = """
            package cn.wubo.loaderutiltest;
                        
            import cn.wubo.loader.util.MethodUtils;
            import org.springframework.web.bind.annotation.GetMapping;
            import org.springframework.web.bind.annotation.RequestMapping;
            import org.springframework.web.bind.annotation.RequestParam;
            import org.springframework.web.bind.annotation.RestController;
                        
            @RestController
            @RequestMapping(value = "test")
            public class DemoController {
                        
                @GetMapping(value = "hello")
                public String testMethod(@RequestParam(value = "name") String name) {
                    return MethodUtils.invokeBean("testClass", "testMethod", name);
                }
            }
            """;

    private static final String jarPath = "./hutool-all/hutool-all-5.8.29.jar";
    private static final String fullClassNameJar = "cn.hutool.core.util.IdUtil";
    private static final String methodNameJar = "randomUUID";

    /**
     * 初始化
     *
     * @return
     */
    @GetMapping(value = "/init")
    public Boolean init() {
        LoaderUtils.clear();
        //编译TestClass
        LoaderUtils.compiler(javaSourceCode, fullClassName);
        //编译DemoController
        LoaderUtils.compiler(javaSourceCodeController, fullClassNameBean);
        //注册testClass的bean
        LoaderUtils.registerSingleton(LoaderUtils.load(fullClassName));
        //注册demoController的rest
        LoaderUtils.registerController(LoaderUtils.load(fullClassNameBean));
        //加载外部jar
        LoaderUtils.addJarPath(jarPath);
        return Boolean.TRUE;
    }

    @GetMapping(value = "/invokeClass")
    public String invokeClass() {
        Class<?> clazz = LoaderUtils.load(fullClassName);
        try {
            Object obj = MethodUtils.proxy(clazz.getConstructor().newInstance());
            return MethodUtils.invokeClass(obj, methodName, "world");
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException |
                 NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
    }

    @GetMapping(value = "/invokeBean")
    public String invokeBean() {
        return MethodUtils.invokeBean("testClass", methodName, "world");
    }

    @GetMapping(value = "/invokeJar")
    public String invokeJar() {
        Class<?> clazz = LoaderUtils.load(fullClassNameJar);
        return (String) MethodUtils.invokeClass(clazz, methodNameJar);
    }
}
